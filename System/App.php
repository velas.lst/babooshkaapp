<?php namespace System;

    class App {


        private $controller_sub_path = 'App/Controllers/';

        public function run() : void {
            $routeString = substr($_SERVER['PATH_INFO'] ?? '/'.DEFAULT_CONTROLLER.'/'.DEFAULT_METHOD, 1);
            $controllerDirectory = $this->checkController($routeString);
            include $controllerDirectory['file'];
            $classAlias = str_replace('.php','', $controllerDirectory['file']);
            $className = str_replace('/','\\', $classAlias);
            $control = explode('\\', $className);
            $control = array_map('ucwords', $control);
            $controllSegments = $control;
            $className = implode('\\', $control);
            $controllerName = array_pop($controllSegments);
            if (!class_exists($className)) {
                die('404, Controlador no encontrado');
            }
            $controller = new $className();
            $querySegments = $this->validateMethod($className, $routeString);
            
            if (!method_exists($controller, $querySegments['mtd'])) {
                if (count($querySegments['params']) == 0) {
                    $querySegments['params'][] = $querySegments['mtd'];
                    $querySegments['mtd'] = DEFAULT_METHOD;
                }
            }
            if (!method_exists($controller, $querySegments['mtd'])) {
                die('404, Metodo no encontrado');
            }
            if (!is_callable([$controller, $querySegments['mtd']])) {
                die('404, Metodo no encontrado');
            }
            $response = $controller->{$querySegments['mtd']}(...$querySegments['params']);
            if ($response instanceof \System\Services\MainStream\ActionResult) {
                echo $response;
            }
        }


        private function validateMethod($controller, $routeString) {
            $auxiliarControl = str_replace($this->controller_sub_path,'',str_replace('\\','/',$controller));
            $mtd = substr($routeString, strlen($auxiliarControl.'/'));
            $mtd = explode('/',$mtd);
            if (isset($mtd[0]) && trim($mtd[0]) == '') {
                $mtd[0] = DEFAULT_METHOD;
            }
            $mtds = $mtd;
            unset($mtds[0]);
            $mtds = array_values($mtds);
            return ['mtd' => ($mtd[0]), 'params' => $mtds];
        }

        private function checkController(string $routeString) : array {
            $validController = false;
            $ControllerRoute = $this->controller_sub_path;
            $file = $ControllerRoute.$routeString.'.php';
            $validController = is_file($file);
            $segs = explode('/', $routeString);
            while ($validController == false && count($segs) > 0) {
                array_pop($segs);
                $file = $ControllerRoute.implode('/', $segs).'.php';
                $validController = is_file($file);
            }
            if ($file == $this->controller_sub_path.'.php') { return ['file' => $this->controller_sub_path.DEFAULT_CONTROLLER.'.php', 'found' => false]; }
            return ['file' => $file, 'found' => true];
        }
    }
?>