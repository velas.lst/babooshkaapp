<?php
    define('APP_NAME', 'Babooshka');
    define('APP_DESCRIPTION', 'JUST ANOTHER EMPTY APP');
    define('APP_VERSION', '1.0');
    define('APP_FOLDER', 'Babooshka');
    define('APP_BASE_ROUTE', $_SERVER['DOCUMENT_ROOT']);
    define('SYSTEM_PATH', APP_BASE_ROUTE.'/System');
    define('APP_PATH', APP_BASE_ROUTE.(APP_FOLDER !== '' ? '/'.APP_FOLDER : ''));
    define('CONTROLLERS_PATH', APP_PATH.'/App/Controllers');
    define('MODELS_PATH', APP_PATH.'/App/Models');
    define('VIEWS_PATH', APP_PATH.'/App/Views');
    define('DEFAULT_METHOD','index');
    define('DEFAULT_CONTROLLER','Home');
    define('ENV','development');
    define('BASE_URL', $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/'.(APP_FOLDER !== '' ? APP_FOLDER.'/' : ''));
