<?php namespace System\Services\MainStream;

class ViewResult implements ActionResult {

    private $baseFolder;
    public $file;
    public $currentSection;
    public $sections;
    public $parent = null;
    public $content;
    public $viewData;
    public $viewBag;
    public $hasParent = false;
    public $parentFile;

    public $yields;

    public function __construct(string $baseFolder) {
        $this->baseFolder = $baseFolder;
        $this->hasParent = false;
    }

    public function __toString() : string {
        return $this->content ?? '';
    }

    public function extends(string $parentFile) {
        $this->parent = new ViewResult($this->baseFolder);
        $this->hasParent = true;
        $this->parentFile = $parentFile;
        $this->parent->viewData = $this->viewData;
        $this->parent->viewBag = $this->viewBag;
        $this->parent->yields = [];
    }
    
    public function yield(string $key) {
        return $this->yields[$key] ?? '';
    }

    public function setYield(string $yield, string $content) {
        $this->parent->yields[$yield] = $content;
    }

    public function renderSection(string $sectionKey) : string {
        return $this->sections[$sectionKey] ?? '';
    }

    public function section(string $sectionKey) {
        ob_start();
        $this->parent->currentSection = $sectionKey;
    }

    public function endSection() {
        $this->parent->sections[$this->parent->currentSection] = ob_get_contents();
        ob_end_clean();
        $this->parent->currentSection = null;
    }

    public function setFile(string $file,int $i = 0) : string {
        $i  = $i += 1;
        ob_start();
        include $this->baseFolder.$file;
        $content = ob_get_contents();
        ob_end_clean();
        if ($this->parent instanceof ViewResult) {
            $content = $this->parent->setFile($this->parentFile, $i);
        }
        $this->content = $content;
        return $this->content;
    }
}
