<?php namespace System\Services\MainStream;

interface ActionResult {

    public function __toString();

}
