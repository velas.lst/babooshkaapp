<?php namespace System\Services\DataPersistence;

class Database {


    public $defaultConnection = 'default';

    public $conections = [
        'default' => [
            'driver' => '\System\Core\Model\Engine\MySql',
            'server' => 'localhost',
            'user' => 'root',
            'password' => '',
            'database' => 'dbbabooshka',
        ],
        'test' => [
            'driver' => '\System\Core\Model\Engine\SqlSrv',
            'server' => 'localhost',
            'user' => 'sa',
            'password' => '*****',
            'database' => 'dbDemo',
        ]
    ];
    
    public $currentSystem;

    public function setDB(string $connectionName) {
        $this->defaultConnection = $connectionName;
        $this->currentSystem = $this->conections[$this->defaultConnection];
    }
}
