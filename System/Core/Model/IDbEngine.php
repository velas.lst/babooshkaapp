<?php namespace System\Core\Model;

interface IDbEngine {

    public function connect(array $config);

    public function runSelect(string $returnType, ?string $field) : array ;
    
    public function getResult(string $returnType);

}