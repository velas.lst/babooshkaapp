<?php namespace System\Core\Model\Engine;

class DB {

    protected $cnx;

    protected $fields;
    protected $table;
    protected $joins = [];
    protected $limit;

    protected $sql;



    public function runSelect(string $returnType = 'array', ?string $field = null) : array {
        $rows = [];
        $stmt = $this->cnx->prepare($this->sql);
        if (in_array($returnType, ['array','object'])) {
            $stmt->setFetchMode($returnType == 'array' ? \PDO::FETCH_ASSOC : \PDO::FETCH_OBJ);
        } else {
            $stmt->setFetchMode(\PDO::FETCH_CLASS, $returnType);
        }
        $stmt->execute();
        while ($r = $stmt->fetch()){ $rows[] = $r; }
        return $rows;
    }


    /**
     * Conectar a una base de datos mediante la extension PDO
     *
     * @param Void 
     * @return type
     * @throws conditon
     **/
    public function connectPDO(string $engine, array $conf, int $errMode = \PDO::ATTR_ERRMODE, int $exMode = \PDO::ERRMODE_EXCEPTION) {
        $this->cnx = new \PDO($engine.':server='.$conf['server'].';database='.$conf['database'], $conf['user'], $conf['password']);
        $this->cnx->setAttribute($errMode, $exMode);
    }

    protected function clearQueryConfiguration() {
        $this->fields = '';
        $this->table = '';
        $this->joins = [];
        $this->limit = '';
    }

    public function select(string $fields) : self {
        $this->fields = $fields;
        return $this;
    }

    public function from(string $table) : self {
        $this->table = $table;
        return $this;
    }

    public function join(string $table, string $on, string $joinType = 'INNER') : self {
        $this->join[] = ['table' => $table, 'on' => $on, 'type' => $joinType];
        return $this;
    }

    public function prepareSelect() : self {
        $this->sql = 'SELECT '.$this->fields." \n";
        $this->sql .= 'FROM '.$this->table." \n";
        foreach ($this->joins as $j) {
            $this->sql .= $j['type'].' JOIN '.$j['table'].' ON '.$j['on']." \n";
        }
        if (trim($this->limit) !== '') { $this->sql .= 'LIMIT '.$this->limit; }
        return $this;
    }
}
