<?php namespace System\Core\Model\Engine;

use System\Core\Model\IDbEngine;

class SqlSrv extends DB implements IDbEngine {

    public function connect(array $config) {
        $this->connectPDO('sqlsrv', $config);
    }

    

    public function getResult(string $returnType = 'array') {
        return null;
    }
    
}