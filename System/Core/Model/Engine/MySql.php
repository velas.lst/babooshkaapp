<?php namespace System\Core\Model\Engine;

use System\Core\Model\IDbEngine;

class MySql extends DB implements IDbEngine {

    public function connect(array $config) {
        $this->cnx = mysqli_connect($config['server'], $config['user'], $config['password'], $config['database']);
    }

    public function runSelect(string $returnType = 'array', ?string $field = null) : array {
        $result = [];
        $cmd = $this->cnx->query($this->sql);
        if ($returnType === 'array') { while ($r = $cmd->fetch_assoc()) { $result[] = $field == null ? $r : $r[$field]; } return $result;  }
        if ($returnType === 'object') { while ($r = $cmd->fetch_object()) { $result[] = $field == null ? $r : $r->{$field}; } return $result; }
        while ($r = $cmd->fetch_assoc()) {
            $obj = new $returnType();
            foreach ($r as $k => $v) { $obj->{$k} = $v; }
            $result[] = $obj;
        }
        return $result;
    }

    public function getResult(string $returnType = 'array') {
        $cmd = $this->cnx->query($this->sql);
        if (in_array($returnType, ['array', 'object'])) {
            return ($returnType = 'array') ? $cmd->fetch_assoc() : $cmd->fetch_object();
        }
        $r = $cmd->fetch_assoc();
        $obj = new $returnType();
        foreach ($r as $k => $v) { $obj->{$k} = $v; }
        return $obj;
    }

    public function runInsert() {

    }

    public function runUpdate() {
        
    }

    public function runDelete() {
        
    }
}