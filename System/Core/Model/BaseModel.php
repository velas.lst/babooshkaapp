<?php namespace System\Core\Model;

class BaseModel {

    public $db;

    protected $dbConfig;

    public function __construct() {
        $this->dbConfig = new \System\Services\DataPersistence\Database();
        $this->setConnection($this->dbConfig->defaultConnection);
    }

    public function setConnection(string $connectionAlias) {
        $this->dbConfig->setDB($connectionAlias);
        $this->db = new $this->dbConfig->currentSystem['driver']();
        $this->db->connect($this->dbConfig->currentSystem);
    }
}
