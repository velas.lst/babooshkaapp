<?php
    include_once 'System/Config/Constants.php';
    include_once 'common/autoload.php';
    include_once 'common/common.php';
    include 'vendor/autoload.php';
    spl_autoload_register('loadModules');
    $babooshka = new System\App();
    $babooshka->run();
    