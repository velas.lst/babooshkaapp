<?php

use System\Services\MainStream\ActionResult;
use System\Services\MainStream\ViewResult;

if (!function_exists('info')) {
        function info() {
            phpinfo();
        }
    }

    if (!function_exists('view')) {
        function view(string $file, array $data = []) : ActionResult{
            $viewResult = new ViewResult(VIEWS_PATH.'/');
            $viewResult->viewData = $data;
            $viewResult->viewBag = (object) $data;
            $viewResult->setFile($file);
            return $viewResult;
        }
    }
?>