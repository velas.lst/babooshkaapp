<?php namespace App\Controllers;

use System\Services\MainStream\ActionResult;
use App\Models\Users;

class Home extends BaseController {

    private $model;

    public function __construct () {
        $this->model = new Users();
    }

    public function test() {
        $demo = $this->model->demostrative();
        foreach ($demo as $d) {
            $d->dump();
        }
    }

    public function index () : ActionResult {
        return view('index.php', ['list' => ['items' => []]]);
    }

    public function list() : ActionResult {
        $records_per_page = 3;
        $list = $this->model->list();
        $pagination = new \Zebra_Pagination();
        $pagination->records(count($list));
        $pagination->records_per_page($records_per_page);
        $list = array_slice($list, (($pagination->get_page() - 1) * $records_per_page), $records_per_page);
        
        return view('index.php', [
            'list' => [    
                'items' => $list,
                'pagination' => $pagination->render(true)
            ]
        ]);
    }

    public function verUsuario(int $id = 0) {
        return view('index.php', [
            'list' => [],
            'user' => 'none'
        ]);
    }
}
