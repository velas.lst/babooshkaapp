<?php namespace App\Entities;
    
    class DemostrativeEnt
    {
        public $id;
        
        public $description;

        public $price;

        public $flag_state;

        public function dump() {
            var_dump($this->id, $this->description, $this->price, $this->flag_state);
            echo '<hr>';
        }
    }
?>