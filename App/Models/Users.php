<?php namespace App\Models;

use System\Core\Model\BaseModel;
class Users extends BaseModel{

    public function __construct() {
        parent::__construct();
    }

    public function demostrative() { // El select basico en mysql ya funciona
        $this->db->select('*');
        $this->db->from('demostrative');
        $this->db->prepareSelect();
        return $this->db->runSelect('\App\Entities\DemostrativeEnt');
    }

    public function list() {
        return ['user1', 'user2', 'user3', 'user4','user5','user6','user7','user8','user9','user10'];
    }

    public function userData(int $id) {
        return [
            'id' => $id,
            'nombres' => 'Victor',
            'Apellidos' => 'Velasquez',
            'edad' => 22,
        ];
    }
}
