<?php $this->extends('templates/base.php'); ?>

<?php $this->setYield('title','Demostración'); ?>
<?php $this->section('content'); ?>
    <h3>Esto es lo que un progrmador inspirado y su IDE pueden hacer juntos</h3>
    <div>HOLA MUNDO</div>
    <?php if (count($this->viewData['list']['items']) > 0) { ?>
        <ul>
            <?php foreach ($this->viewData['list']['items'] as $item) { ?>
                <li><?= $item; ?></li>
            <?php } ?>
        </ul>
        <?= $this->viewData['list']['pagination']; ?>
    <?php } ?>
<?php $this->endSection(); ?>